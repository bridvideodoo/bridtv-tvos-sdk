//
//  AdMacros.h
//  BridSDK
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2019 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdMacros : NSObject

@property (nonatomic, strong, nullable) NSString *userAgent;
@property (nonatomic, strong, nullable) NSString *referrerUrl;
@property (nonatomic, strong, nullable) NSString *pageUrl;
@property (nonatomic, strong, nullable) NSString *appStoreUrl;

- (instancetype _Nullable )initAdMacrosUserAgent:(NSString *_Nullable)userAgent
                           referrerUrl:(NSString *_Nullable)referrerUrl
                          pageUrl:(NSString *_Nullable)pageUrl
                           appStoreUrl:(NSString *_Nullable)appStoreUrl;
@end
