//
//  BVPlayer.h
//  BridSDK
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <BridSDKtvOS/BVData.h>
#import <BridSDKtvOS/BVAdData.h>
#import <AVFoundation/AVFoundation.h>

@interface BVPlayer: NSObject

- (instancetype _Nullable)init __deprecated __deprecated_msg("This will create empty player, without data.");
- (instancetype _Nullable)initWithData:(BVData *_Nonnull)baseData forAvController:(AVPlayerViewController *_Nullable)avController;
- (void)contentProposal:(BOOL)useContentProposal;
- (void)play;
- (void)pause;
- (void)mute;
- (void)unmute;
- (void)previous;
- (void)next;
- (void)stop;


// MARK: Get Methods
- (CMTime)getCurrentTime;
- (void)seekToTime:(CMTime)time;

@end
