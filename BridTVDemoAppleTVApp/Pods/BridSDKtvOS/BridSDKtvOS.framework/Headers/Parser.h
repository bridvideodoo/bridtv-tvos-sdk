//
//  Parser.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parser : NSObject

@property (nonatomic, readonly, strong, nullable) NSDictionary *asDictionary;

- (instancetype _Nullable )initWithJSON:(NSDictionary *_Nullable) parser;
+ (id _Nullable )createDataObject:(NSDictionary *_Nullable)json forName:(NSString *_Nullable)name;

@end
