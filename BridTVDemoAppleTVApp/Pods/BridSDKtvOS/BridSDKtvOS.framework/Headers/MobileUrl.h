//
//  MobileUrl.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BridSDKtvOS/Mobile.h>
#import <BridSDKtvOS/Parser.h>

@interface MobileUrl : Parser

@property (nonatomic, readonly, strong, nullable) NSArray<Mobile *> *mobile;

@end
