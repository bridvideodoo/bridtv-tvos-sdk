//
//  Source.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@interface Source : Parser

@property (nonatomic, strong, nullable) NSString *streaming;
@property (nonatomic, strong, nullable) NSString *ld;
@property (nonatomic, strong, nullable) NSString *sd;
@property (nonatomic, strong, nullable) NSString *hsd;
@property (nonatomic, strong, nullable) NSString *hd;
@property (nonatomic, strong, nullable) NSString *fhd;



//@property (nonatomic, readonly, strong, nullable) NSString *ogg;
//@property (nonatomic, readonly, strong, nullable) NSString *mp3;

@end
