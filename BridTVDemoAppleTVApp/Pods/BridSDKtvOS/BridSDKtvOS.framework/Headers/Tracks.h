//
//  TracksModel.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"


@interface Tracks : Parser

@property (nonatomic, readonly, strong, nullable) NSString *kind;
@property (nonatomic, readonly, strong, nullable) NSString *src;

@end
