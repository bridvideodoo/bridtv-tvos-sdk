//
//  UrchinFields.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@interface UrchinFields : Parser

@property (nonatomic, readonly, strong, nullable) NSString *ga_start;
@property (nonatomic, readonly, strong, nullable) NSString *ga_play;
@property (nonatomic, readonly, strong, nullable) NSString *ga_pause;
@property (nonatomic, readonly, strong, nullable) NSString *ga_end;
@property (nonatomic, readonly, strong, nullable) NSString *first_qt;
@property (nonatomic, readonly, strong, nullable) NSString *second_qt;
@property (nonatomic, readonly, strong, nullable) NSString *third_qt;
@property (nonatomic, readonly, strong, nullable) NSString *ad_req;
@property (nonatomic, readonly, strong, nullable) NSString *ad_imp;
@property (nonatomic, readonly, strong, nullable) NSString *ga_social;

@end
