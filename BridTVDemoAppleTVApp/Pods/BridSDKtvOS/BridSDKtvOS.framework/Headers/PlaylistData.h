//
//  Playlist.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@interface PlaylistData : Parser

@property (nonatomic, readonly, strong, nullable) NSString *_id;
@property (nonatomic, readonly, strong, nullable) NSString *name;
@property (nonatomic, readonly, strong, nullable) NSNumber *items;
@property (nonatomic, readonly, strong, nullable) NSNumber *page;
@property (nonatomic, readonly, strong, nullable) NSNumber *limit;
@property (nonatomic, readonly, strong, nullable) NSString *exclude_tag;

@end
