//
//  BVMobileData.h
//  BridSDKtvOS
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Xcode Peca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BVMobileData: NSObject

@property (strong, nonatomic, readonly, nullable) NSString *idUrl;
@property (strong, nonatomic, readonly, nullable) NSString *adURL;

- (instancetype _Nonnull)initWithID:(NSString *_Nullable)idUrl url:(NSString *_Nullable)adURL;

@end
