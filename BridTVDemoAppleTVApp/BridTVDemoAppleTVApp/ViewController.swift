//
//  ViewController.swift
//  BridTVDemoAppleTVApp
//
//  Created by Xcode Peca on 27/10/2020.
//

import UIKit
import BridSDKtvOS


class ViewController: UIViewController {
    
    @IBOutlet weak var embededLabel: UILabel!
    @IBOutlet weak var smallPlayerView: UIView!
    
    var player: BVPlayer?
    var playerController: AVPlayerViewController?
    var currentTime: CMTime?
    var isSeek: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupEventNetworking()
        
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.allowedPressTypes = [NSNumber(value: UIPress.PressType.leftArrow.rawValue)];
        tapRecognizer.allowedPressTypes = [NSNumber(value: UIPress.PressType.rightArrow.rawValue)];
        self.view.addGestureRecognizer(tapRecognizer)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentTime = player?.getCurrentTime()
        player = nil
        self.playerController = nil
    }
    
    override func pressesEnded(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        for press in presses {
            if (press.type == .leftArrow) {
                player?.previous()
            }  else if (press.type == .rightArrow){
                player?.next()
            }
        }
    }

    
    @IBAction func playViewTapped(_ sender: Any) {
        player = nil
        self.playerController = AVPlayerViewController.init()
        player = BVPlayer(data: BVData(playerID: 24282, forVideoID: 442011), forAvController: self.playerController)
        self.present(playerController!, animated: true, completion: nil)
    }
    
    @IBAction func playlistPlayerTapped(_ sender: Any) {
        player = nil
        self.playerController = AVPlayerViewController.init()
        player = BVPlayer(data: BVData(playerID: 24282, forPlaylistID: 13735), forAvController: self.playerController)
        self.present(playerController!, animated: true, completion: nil)
    }
    
    @IBAction func seekToTime(_ sender: Any) {
        self.playerController = AVPlayerViewController.init()
        player = BVPlayer(data: BVData(playerID: 24282, forVideoID: 442011), forAvController: self.playerController)
        self.present(playerController!, animated: true, completion: nil)
        isSeek = true
    }
    
    @IBAction func playVideoInViewTapped(_ sender: Any) {
        player = nil
        self.playerController = AVPlayerViewController.init()
        player = BVPlayer(data: BVData(playerID: 24282, forVideoID: 442011), forAvController: self.playerController)
        self.playerController!.view.frame = smallPlayerView.bounds
        self.smallPlayerView.addSubview(playerController!.view)
    }
    
    func setupEventNetworking() {
           NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "PlayerEvent"), object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "AdEvent"), object: nil)
    }
    
    @objc func eventWriter(_ notification: NSNotification) {
        
        var event: String?
        
        if notification.name.rawValue == "PlayerEvent" {
            event = notification.userInfo!["event"] as? String
            if (event == "playerVideoInitialized") {
                if isSeek ?? false {
                    player?.seek(to: currentTime!)
                    isSeek = false
                }
            }
            
            print("events:\(event!)")
        }
        
        if notification.name.rawValue == "AdEvent" {
            event = notification.userInfo!["ad"] as? String
            print("events:\(event!)")
        }
    }
}

