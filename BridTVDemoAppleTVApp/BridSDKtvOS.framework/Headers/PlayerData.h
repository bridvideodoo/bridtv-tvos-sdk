//
//  PlayerData.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"
#import <BridSDKtvOS/Branding.h>

@interface PlayerData : Parser

@property (nonatomic, readonly, strong, nullable) NSString *partner_id;
@property (nonatomic, readonly, assign) BOOL vpaid_controls;
@property (nonatomic, readonly, assign) BOOL vpaid_advertisement;
@property (nonatomic, readonly, strong, nullable) NSString *playButtonColor;
@property (nonatomic, readonly, strong, nullable) NSString *playButtonImg;
@property (nonatomic, readonly, assign) BOOL titleBar;
@property (nonatomic, readonly, assign) BOOL slide_inview;
@property (nonatomic, readonly, assign) BOOL slide_inview_mobile;
@property (nonatomic, readonly, strong, nullable) NSString *width;
@property (nonatomic, readonly, strong, nullable) NSString *height;
@property (nonatomic, readonly, assign) BOOL monetize;
@property (nonatomic, readonly, assign) BOOL ad_preload;
@property (nonatomic, readonly, assign) BOOL ad_preload_full;
@property (nonatomic, readonly, assign) BOOL autoplay;
@property (nonatomic, readonly, assign) BOOL autoplay_desktop;
@property (nonatomic, readonly, assign) BOOL autoplay_on_ad;
@property (nonatomic, readonly, assign) BOOL autohide;
@property (nonatomic, readonly, assign) BOOL forceMuted;
@property (nonatomic, readonly, assign) BOOL deblocker;
@property (nonatomic, readonly, assign) BOOL autoplayInview;
@property (nonatomic, readonly, assign) BOOL autoplayInviewPct;
@property (nonatomic, readonly, strong, nullable) NSString *slide_inview_show;
@property (nonatomic, readonly, strong, nullable) NSString *mobile_width_inview;
@property (nonatomic, readonly, strong, nullable) NSString *slide_inview_seconds;
@property (nonatomic, readonly, strong, nullable) NSString *share;
@property (nonatomic, readonly, assign) BOOL debug;
@property (nonatomic, readonly, assign) BOOL volume;
@property (nonatomic, readonly, assign) BOOL fullscreen;
@property (nonatomic, readonly, assign) BOOL controls;
@property (nonatomic, readonly, strong, nullable) NSString *videoPlayback;
@property (nonatomic, readonly, strong, nullable) NSString *playlistPlayback;
@property (nonatomic, readonly, strong, nullable) NSString *afterPlaylistEnd;
@property (nonatomic, readonly, strong, nullable) NSString *adFrequency;
@property (nonatomic, readonly, strong, nullable) NSString *adOffset;
@property (nonatomic, readonly, strong, nullable) NSString *comscore;
@property (nonatomic, readonly, assign) BOOL pauseOffView;
@property (nonatomic, readonly, assign) BOOL pauseAdOffView;
@property (nonatomic, readonly, strong, nullable) NSNumber *start_volume;
@property (nonatomic, readonly, strong, nullable) NSNumber *hover_volume;
@property (nonatomic, readonly, assign) BOOL act_as_outstream;
@property (nonatomic, readonly, assign) BOOL act_as_outstream_inslide;
@property (nonatomic, readonly, strong, nullable) NSString *inslide_direction;
@property (nonatomic, readonly, assign) BOOL inpage_mobile;
@property (nonatomic, readonly, assign) BOOL inpage_mobile_clicktoplay;
@property (nonatomic, readonly, assign) BOOL playlistScreen;
@property (nonatomic, readonly, assign) BOOL shareScreen;
@property (nonatomic, readonly, assign) BOOL cogwheel;
@property (nonatomic, readonly, assign) BOOL backfill_monetize;
@property (nonatomic, readonly, strong, nullable) NSString *c3;
@property (nonatomic, readonly, strong, nullable) NSString *bid_platform;
@property (nonatomic, readonly, assign) BOOL encode_preroll;
@property (nonatomic, readonly, assign) BOOL engagement_booster;
@property (nonatomic, readonly, strong, nullable) NSString *_id;
@property (nonatomic, readonly, strong, nullable) NSString *primaryColor;
@property (nonatomic, readonly, strong, nullable) NSArray<NSString *> *adSDK;
@property (nonatomic, readonly, strong, nullable) NSString *mode;
@property (nonatomic, readonly, strong, nullable) NSString *owner_id;
@property (nonatomic, readonly, strong, nullable) Branding *branding;
@property (nonatomic, readonly, assign) BOOL inCms;

@end


