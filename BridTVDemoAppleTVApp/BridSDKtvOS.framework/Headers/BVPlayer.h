//
//  BVPlayer.h
//  BridSDK
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <BridSDKtvOS/BVData.h>
#import <BridSDKtvOS/BVAdData.h>
#import <AVFoundation/AVFoundation.h>

@interface BVPlayer: NSObject

- (instancetype _Nullable)init __deprecated __deprecated_msg("This will create empty player, without data.");
- (instancetype _Nullable)initWithData:(BVData *_Nonnull)baseData forAvController:(AVPlayerViewController *_Nullable)avController;
- (void)contentProposal:(BOOL)useContentProposal;
- (void)play;
- (void)pause;
- (void)mute;
- (void)unmute;
- (void)previous;
- (void)next;
- (void)stop;
//- (void)autoFullscreenWhenRotating:(BOOL)rotate;
//- (void)setToFullscreen;


// MARK: Get Methods
//- (double)getCurrentTime;
//- (double)getDuration;
//- (float)getVolume;
//- (BOOL)isAdInProgress;
//
//- (NSInteger)getCurrentIndex;
//- (NSArray<VideoData *> *_Nullable)getPlaylist;
//- (NSString *_Nullable)getSource;
//- (VideoData *_Nullable)getVideo;

// MARK: Set Methods
//- (void)setVolume:(float)volume;
//- (void)playByIndex:(NSInteger)index;
////- (void)instantPlayVideo:(VideoData *)video;
//- (void)setVideo:(VideoData *_Nullable)video;
//- (void)setVideoUrl:(NSString *_Nullable)url;
//- (void)setPlaylist:(NSURL *_Nullable)url;
//- (void)setAd:(NSArray<AdData *> *_Nullable)ad;
//- (void)playAd:(AdData *_Nullable)ad;
//- (void)playAdTagUrl:(NSURL *_Nullable)url;
//- (void)setAdMacros:(AdMacros *_Nullable)macros;
//- (void)autoHideControls:(BOOL)isHidden;

- (void)setContainerView:(UIView * _Nullable)containerView;
- (void)registerVideoOverlay:(UIView *_Nullable)adContainer companionSlots:(NSArray *_Nullable)companionSlots;

@property (readonly, strong, nonatomic) UIView *_Nullable adContainer;
@property (readonly, copy, nonatomic) NSArray *_Nullable companionSlots;

@end
