//
//  ConfigurationData.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BridSDKtvOS/Parser.h>
#import <BridSDKtvOS/PlayerData.h>
#import <BridSDKtvOS/PartnerData.h>
#import <BridSDKtvOS/VideoData.h>
#import <BridSDKtvOS/AdData.h>
#import <BridSDKtvOS/ChannelData.h>
#import <BridSDKtvOS/PlaylistData.h>
#import <BridSDKtvOS/ErrorData.h>
#import <BridSDKtvOS/AdMacros.h>

@interface ConfigurationData : Parser

@property (nonatomic, readonly, strong) PlayerData *Player;
@property (nonatomic, readonly, strong) PartnerData *Patner;
@property (nonatomic, readonly, strong) NSArray<VideoData *> *Video;
@property (nonatomic, strong) NSArray<AdData *> *Ad;
@property (nonatomic, readonly, strong) ChannelData *Channel;
@property (nonatomic, readonly, strong) PlaylistData *Playlist;
@property (nonatomic, readonly, strong) ErrorData *Error;


- (instancetype)initWithJSON:(NSDictionary*)parser;
- (instancetype) initWithPlayer:(PlayerData*)player
                         patner:(PartnerData*)patner
                          video:(NSArray<VideoData*>*)video
                             ad:(NSArray<AdData*>*)ad
                        channel:(ChannelData*)channel
                       playlist:(PlaylistData*)playlist
                          error:(ErrorData*)error;

@end
