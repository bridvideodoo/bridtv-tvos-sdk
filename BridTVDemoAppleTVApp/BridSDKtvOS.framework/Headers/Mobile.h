//
//  Mobile.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"


@interface Mobile : Parser

@property (nonatomic, readonly, strong, nullable) NSString *_id;
@property (nonatomic, readonly, strong, nullable) NSString *url;

- (instancetype _Nullable )initWithMobile:(NSString *_Nullable)_id
                                 url:(NSString *_Nullable)url;

@end
