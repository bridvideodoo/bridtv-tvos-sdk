//
//  Program.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BridSDKtvOS/MobileUrl.h>
#import "Parser.h"

@interface Program : Parser

@property (nonatomic, readonly, strong, nullable) NSArray<Mobile*> *mobile;

@end
