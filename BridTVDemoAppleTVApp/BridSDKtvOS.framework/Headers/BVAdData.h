//
//  BVAdData.h
//  BridSDKtvOS
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Xcode Peca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BridSDKtvOS/BVMobileData.h>

@interface BVAdData : BVMobileData

@property (strong, nonatomic, readonly, nullable) NSArray<NSArray<BVMobileData*>*> *mobileDataArray;


- (instancetype _Nonnull)initURLs:(NSArray<NSArray<BVMobileData*>*> *_Nullable)mobileDataArray forRollType:(float)type;
- (float)rollType;

@end
