//
//  BVData.h
//  BridSDK
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BridSDKtvOS/BVObject.h>
#import <BridSDKtvOS/ConfigurationData.h>

typedef enum : NSUInteger
{
    BVDataTypeVideo,
    BVDataTypePlaylist,
    BVDataTypeLatest,
    BVDataTypeChannel,
    BVDataTypeTag,
    BVDataRelated
} BVDataType;

@interface BVData: BVObject
@property (nonatomic, readonly) int playerID;
@property (nonatomic, readonly) int typeID;
@property (nonatomic, readonly, nonnull) NSString *tagType;
@property (nonatomic, readonly) int page;
@property (nonatomic, readonly) int item;
@property (nonatomic, readonly) int tagId;
@property (nonatomic, readonly) BVDataType type;
@property (nonatomic, readonly, nullable) ConfigurationData *configurationData;
- (instancetype _Nonnull)initPlayerID:(int)player forVideoID:(int)video;
- (instancetype _Nonnull)initPlayerID:(int)player forPlaylistID:(int)playlist;
- (instancetype _Nonnull)initPlayerID:(int)player forLatestID:(int)listID page:(int)page item:(int)item;
- (instancetype _Nonnull)initPlayerID:(int)player forChanneltID:(int)channelID page:(int)page item:(int)item;
- (instancetype _Nonnull)initPlayerID:(int)player forTag:(NSString *_Nonnull)tag page:(int)page item:(int)item;
- (instancetype _Nonnull)initPlayerID:(int)player relatedVideoID:(int)video page:(int)page item:(int)item;
@end
