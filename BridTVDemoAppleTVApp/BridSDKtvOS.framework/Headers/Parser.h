//
//  Parser.h
//  PlayerForIMATestObjc
//
//  Created by Xcode Peca on 27/07/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parser : NSObject

@property (nonatomic, readonly, strong, nullable) NSDictionary *asDictionary;

- (instancetype)initWithJSON:(NSDictionary *) parser;
+ (id)createDataObject:(NSDictionary *)json forName:(NSString *)name;

@end
