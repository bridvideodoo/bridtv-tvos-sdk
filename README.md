# BridSDK tvOS Example

This SDK aims at easily playing videos with or without ads in your tvOS application. Its was written in objective c, and supports swift and objective c programming language.   
Add pod 'BridSDKtvOS' to yours pod file.  
This example depends on GoogleAds-IMA-tvOS-SDK, so it was inserted through pods.

* Dependency: GoogleAds-IMA-tvOS-SDK

## Example application

Integration of the Brid SDK is very simple. 

BridSDK gives you the factory methods to pair the player with your player id that you set up in BRID CMS.
Each Player (BVPlayer), is created with the help of your credentials (which includes the ID of your player, playlist ID etc.
BVPlayer replace created AVPlayerViewController you have at your disposal, it is now on you to:

1) Declare AVPlayerViewController and BVPlayer object

2) Setup a BVObject data object where you can configure a BVPlayer instance using your credentials


## You can choose the type of BVData:

### Player with single video

Swift:``BVData(playerID: Int32, forVideoID: Int32)`` 

Objc:``[[BVData alloc] initPlayerID:<int> forVideoID:<int>]``

### Player with playlist

Swift:``BVData(playerID: Int32, forPlaylistID: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forPlaylistID:<int>]``

### Player with playlist by channel and option from what page and how many to load


Swift:``BVData(playerID: Int32, forChanneltID: Int32, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forChanneltID:<int> page:<int> item:<int>]``

### Player with latest videos playlist and option from what page and how many to load

Swift:``BVData(playerID: Int32, forLatestID: Int32, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forLatestID:<int> page:<int> item:<int>]``

### Player with a video by tag playlist, it loads video with tag string and option from what page and how many to load

Swift:``BVData(playerID: Int32, forTag: String, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forTag:<String> page:<int> item:<int>]``




---

# How to use

### Declare a player variable:

### Swift:  
``var player: BVPlayer?``

### Objc:  
``@property (nonatomic, strong) BVPlayer player;``  

### Initialize player with object:

### Swift:  
``player = BVPlayer(data: BVData(playerID: <Int32>, forVideoID: <Int32>), forAvController: <AVPlayerViewController>)``

### Objc:  
``player = [[BVPlayer alloc] initWithData:[[BVData alloc] initPlayerID:<int> forVideoID:<int>]  forAvController:<AVPlayerViewController>]``

### Present player with object:

### Swift:  
``self.presentViewController(player, animated: true, completion: nil)``

### Objc:  
``[self presentViewController:player animated:YES completion:nil];``


### Listen to events from player:

You can listen to events of the player through the Notification Centre. There are player events and ad events. Just change the string in notification.usreInfo.
 
### Swift: 
```
func setupEventNetworking() {  
       NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "PlayerEvent"), object: nil) 
	   NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "AdEvent"), object: nil) 
}
@objc func eventWriter(_ notification: NSNotification) {       
	if notification.name.rawValue == "PlayerEvent" {
		print(notification.userInfo?["event"] as! String)
   	}  
 	if notification.name.rawValue == "AdEvent" {   
		print(notification.userInfo?["ad"] as! String)
    }   
}
```

### Objc:
```
- (void)setupEventNetworking {   
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventWriter:) name:@"PlayerEvent" object:nil];   
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventWriter:) name:@"AdEvent" object:nil];    
}  
- (void) eventWriter:(NSNotification *)notification {
	if ([notification.name isEqualToString:@"PlayerEvent"]) {  
    	[self logMessage:(NSString *)notification.userInfo[@"event"]]; 
	}  
	if ([notification.name isEqualToString:@"AdEvent"]) { 
    	[self logMessage:(NSString *)notification.userInfo[@"ad"]];  
	}  
}  
```

## BridSDK player example methods:

### Swift:	 
``player.play()`` 					
``player.pause()`` 				
``player.mute()`` 					
``player.unmute()``               
``player.next()``                     
``player.previous()``                                         
``player.stop()``                                  

### Objc:	  
``[self.player play];`` 			
``[self.player pause];`` 			
``[self.player mute];`` 			
``[self.player unmute];`` 		     
``[self.player next];`` 		     
``[self.player previous];`` 		     
``[self.player stop];`` 			   

---

## Requirements

	•	XCode10 or newer
	•	iOS 11.0+
	•	CocoaPods



